<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
  .bg-interna{
    background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <div class="col-12 top70">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>NOSSAS DICAS</h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>


  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->
  <div class="row dicas_home bottom50">

    <?php
    $result = $obj_site->select("tb_dicas");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>

        <div class="col-12 top40">
          <div class="card">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <amp-img
              class="p-3"
              layout="responsive"
              height="200"
              width="280"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
            </amp-img>
            <div class="card-body">
              <div>
                <h2 class="text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h2>
              </div>
              <div class=" desc top10">
                <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
              </div>

            </div>


          </a>
        </div>
      </div>

      <?php
      if ($i==0) {
        echo "<div class='clearfix'>  </div>";
      }else {
        $i++;
      }
    }
  }
  ?>

</div>
<!--  ==============================================================  -->
<!--   DICAS -->
<!--  ==============================================================  -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
