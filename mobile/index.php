
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>




</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="156" width="480"></amp-img>
    </div>
  </div>

<?php /*
  <div class=" row font-index ">
    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
    <div class="col-9">
      <p><?php Util::imprime($row[descricao],60); ?></p>
    </div>
  </div>

*/ ?>
      <!-- ======================================================================= -->
      <!--  MENU -->
      <!-- ======================================================================= -->
      <div class="row top50">

        <div class="col-4 top50">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>

        <div class="col-4 top50">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/tratamentos">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_tratamentos_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>

        <div class="col-4 top50">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/clinicas">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_clinica_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>

        <div class="col-4 top30">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dicas">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_dicas_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>

        <div class="col-4  top30">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/fale-conosco">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_fale_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>



        <div class="col-4 top30">
          <a href="<?php Util::imprime($config[src_place]); ?>" target="_blank">
            <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_onde_geral.png"
              width="77"
              height="70"
              layout="responsive"
              alt="AMP">
            </amp-img>
          </a>
        </div>


      </div>
      <!-- ======================================================================= -->
      <!--  MENU -->
      <!-- ======================================================================= -->



</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
