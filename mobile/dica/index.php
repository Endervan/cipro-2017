
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 16); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 83px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>





</head>

<body class="bg-interna">


  <?php
  $voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php") ?>

	<div class="row">
		<div class="col-12 text-center top110 bottom50 titulo_dentro">
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
			<h6><?php echo Util::imprime($dados_dentro[titulo]) ?></h6>
			<amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
		</div>
	</div>

  <!-- ======================================================================= -->
  <!--  barra titulo -->
  <!-- ======================================================================= -->
  <div class="row relativo top10">

		<div class="col-12">
			<amp-img
			on="tap:lightbox1"
			role="button"
			tabindex="0"

			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
			layout="responsive"
			width="450"
			height="400"
			alt="<?php echo Util::imprime($dados_dentro[titulo]) ?>">

		</amp-img>
		<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

	</div>

  </div>
  <!-- ======================================================================= -->
  <!--  barra titulo -->
  <!-- ======================================================================= -->




<!-- ======================================================================= -->
<!--  DICAS DETALHE -->
<!-- ======================================================================= -->
<div class="row bg_cinza bottom50 top20">
  <div class="col-12 empresa_descricao">
    <p ><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
  </div>
</div>
<!-- ======================================================================= -->
<!--  DICAS DETALHE -->
<!-- ======================================================================= -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
