
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
  .bg-interna{
    background: #f5f5f5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 82px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>



</head>

<body class="bg-interna">


  <?php
  require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 localizacao-pagina">
      <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><?php Util::imprime($row1[titulo]); ?></h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>



<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="row empresa_descricao cat_produtos_geral bottom50">

  <div class="col-12">
    <div class="top20">
      <p><?php Util::imprime($row1[descricao]); ?></p>
    </div>
  </div>

  <?php

  $ids = explode("/", $_GET[get1]);

  ?>

  <?php

  //  busca os produtos sem filtro
  $result = $obj_site->select("tb_categorias_produtos", $complemento);

  if(mysql_num_rows($result) == 0){
    echo "<h2 class='btn_formulario clearfix'>Nenhum produto encontrado.</h2>";
  }else{
    ?>
    <div class="col-12 total-resultado-busca top10 bottom10">
      <h1><span><?php echo mysql_num_rows($result) ?> PRODUTO(S) ENCONTRADO(S) . </span></h1>
    </div>

    <?php
    require_once('../includes/lista_tratamentos.php');

  }
  ?>

</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->

<?php require_once("../includes/rodape.php") ?>

</body>



</html>
