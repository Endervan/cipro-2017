<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
  .bg-interna{
    background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
    <div class="col-12 localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><?php Util::imprime($row1[titulo]) ?></h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>






  <div class="row top20">

    <div class="col-12 empresa_descricao">
      <p><?php Util::imprime($row1[descricao]) ?></p>

    </div>

</div>


<div class="row empresa_descricao">
  <?php $row = $obj_site->select_unico("tb_empresa", "idempresa",3);?>
  <div class="col-12 top30">
    <h5 class="mt-0"><?php Util::imprime($row[titulo]); ?></h5>
    <div class="media top30">
      <amp-img
      class="d-flex align-self-start mr-1"
      src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa02.png"
      width="100"
      height="100"
      alt="clinica01">
    </amp-img>
    <div class="media-body">
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>
  </div>
</div>

<?php $row = $obj_site->select_unico("tb_empresa", "idempresa",4);?>
<div class="col-12 top30">
  <h5 class="mt-0"><?php Util::imprime($row[titulo]); ?></h5>
  <div class="media top30">
    <amp-img
    class="d-flex align-self-start mr-1"
    src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa01.png"
    width="100"
    height="100"
    alt="clinica01">
  </amp-img>
  <div class="media-body">
    <p><?php Util::imprime($row[descricao]); ?></p>
  </div>
</div>
</div>

<?php $row = $obj_site->select_unico("tb_empresa", "idempresa",5);?>
<div class="col-12 top30">
  <h5 class="mt-0"><?php Util::imprime($row[titulo]); ?></h5>
  <div class="media top30">
    <amp-img
    class="d-flex align-self-start mr-1"
    src="<?php echo Util::caminho_projeto() ?>/imgs/icon_empresa03.png"
    width="100"
    height="100"
    alt="clinica01">
  </amp-img>
  <div class="media-body">
    <p><?php Util::imprime($row[descricao]); ?></p>
  </div>
</div>
</div>


</div>




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
