<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18); ?>
  .bg-interna{
    background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
  <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">
    <?php $row1 = $obj_site->select_unico("tb_empresa", "idempresa", 9);?>
    <div class="col-12 localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6><?php Util::imprime($row1[titulo]) ?></h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>






  <div class="row top20">

    <div class="col-12">
      <amp-carousel id="carousel-with-preview"
      width="430"
      height="365"
      layout="responsive"
      type="slides"
      autoplay
      delay="4000"
      >

      <?php
      $i = 0;
      $result = $obj_site->select("tb_banners", "AND tipo_banner = 3 ORDER BY rand() LIMIT 6");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          ?>


          <amp-img
          on="tap:lightbox1"
          role="button"
          tabindex="0"

          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
          layout="responsive"
          width="450"
          height="365"
          alt="<?php echo Util::imprime($row[titulo]) ?>">

        </amp-img>
        <?php
      }
      $i++;
    }
    ?>

  </amp-carousel>
  <amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>
</div>

<div class="col-12">
  <div class="top10">
    <p><?php Util::imprime($row1[descricao]); ?></p>
  </div>
</div>
</div>










<div class="row">
  <?php $row = $obj_site->select_unico("tb_empresa", "idempresa",6);?>
  <div class="col-12 top50">
    <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
    <div class="media top20">
      <amp-img
      class="d-flex align-self-start mr-1"
      src="<?php echo Util::caminho_projeto() ?>/imgs/icon_clinicas01.png"
      width="100"
      height="100"
      alt="clinica01">
    </amp-img>
    <div class="media-body">
      <p><?php Util::imprime($row[descricao]); ?></p>
    </div>
  </div>
</div>

<?php $row = $obj_site->select_unico("tb_empresa", "idempresa",7);?>
<div class="col-12 top30">
  <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
  <div class="media top20">
    <amp-img
    class="d-flex align-self-start mr-1"
    src="<?php echo Util::caminho_projeto() ?>/imgs/icon_clinicas02.png"
    width="100"
    height="100"
    alt="clinica01">
  </amp-img>
  <div class="media-body">
    <p><?php Util::imprime($row[descricao]); ?></p>
  </div>
</div>
</div>


</div>


<!-- ======================================================================= -->
<!-- CLIENTES -->
<!-- ======================================================================= -->
<div class="row top30">
  <?php /*
  <!-- <div class="col-12">
    <h5>NOSSOS PARCEIROS</h5>
    <amp-img class="top20 " src="<?php// echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
  </div> -->
*/ ?>
  <div class="col-12">
    <amp-carousel
    height="100"
    layout="fixed-height"
    type="carousel"
    autoplay
    delay="4000"
    >

    <?php
    $i = 0;
    $result = $obj_site->select("tb_clientes");
    if (mysql_num_rows($result) > 0) {
      while($row = mysql_fetch_array($result)){
        ?>

        <amp-img
        on="tap:lightbox2"
        role="button"
        tabindex="0"
        src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
        width="150"
        height="100"
        alt="<?php echo Util::imprime($row[titulo]) ?>">
      </amp-img>
      <?php
    }
    $i++;
  }
  ?>

</amp-carousel>
<amp-image-lightbox id="lightbox2"layout="nodisplay">	</amp-image-lightbox>
</div>


</div>
<!-- ======================================================================= -->
<!-- CLIENTES -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->
<div class="row mapa bottom50 top20">

  <div class="col-12">
    <h5>ENCONTRE NOSSA CLÍNICA </h5>
    <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>

    <p><?php Util::imprime($config[endereco]); ?></p>
  </div>

  <div class="col-12 top10">
    <div class="row">
      <p><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></p>
      <p><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></p>
    </div>
  </div>

  <div class="col-12 top5 padding0">
    <amp-iframe
    width="480"
    height="300"
    layout="responsive"
    sandbox="allow-scripts allow-same-origin allow-popups"
    frameborder="0"
    src="<?php Util::imprime($config[src_place]); ?>">
  </amp-iframe>
</div>
</div>
<!-- ======================================================================= -->
<!-- mapa   -->
<!-- ======================================================================= -->




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
