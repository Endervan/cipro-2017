<div class="row">
    <div class="separador-rodape clearfix"></div>
    <div class="rodape col-12 padding0">

        <div class="col-5 top5">
            <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
              <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/ligue_rodape.png"
                width="250"
                height="25"
                layout="responsive"
                alt="AMP">
              </amp-img>
              </a>
        </div>

        <div class="col-7 top2">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
              <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/consultar_rodape.png"
                width="260"
                height="25"
                layout="responsive"
                alt="AMP">
              </amp-img>

            </a>
        </div>
    </div>
</div>
