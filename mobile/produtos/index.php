
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo",4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
  .bg-interna{
    background: #f5f5f5 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 82px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
  <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>



</head>

<body class="bg-interna">

  <?php
	$voltar_para = 'tratamentos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php") ?>


  <?php
  require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 titulo_produto">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <?php
      $ids = explode("/", $_GET[get1]);


      //  FILTRA AS CATEGORIAS
      if (isset( $ids[0] )) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $ids[0]);
        $complemento_cat .= "AND idcategoriaproduto = '$id_categoria' ";
      }
      $result = $obj_site->select("tb_categorias_produtos", $complemento_cat);
        $dados_dentro_cat = mysql_fetch_array($result);
        ?>

      <h6>TRATAMENTOS PARA <span>  <?php Util::imprime( Util::troca_value_nome($dados_dentro_cat[idcategoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")); ?></span></h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>




<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="row bottom50">

  <?php

  $ids = explode("/", $_GET[get1]);


  //  FILTRA AS CATEGORIAS
  if (!empty( $ids[0] )) {
    $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $ids[0]);
    $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
  }


  //  FILTRA AS SUBCATEGORIAS
  if (isset( $ids[1] )) {
    $id_subcategoria = $obj_site->get_id_url_amigavel("tb_subcategorias_produtos", "idsubcategoriaproduto", $ids[1]);
    $complemento .= "AND id_subcategoriaproduto = '$id_subcategoria' ";
  }

  //  FILTRA PELO TITULO
  if(isset($_POST[busca_produtos])):
    $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
  endif;

  ?>

  <?php

  //  busca os produtos sem filtro
  $result = $obj_site->select("tb_produtos", $complemento);

  if(mysql_num_rows($result) == 0){
    echo "<h2 class='btn_formulario clearfix'>Nenhum produto encontrado.</h2>";
  }else{
    ?>
    <div class="col-12 total-resultado-busca top45 bottom10">
      <h1><span><?php echo mysql_num_rows($result) ?> TRATAMENTO(S) ENCONTRADO(S) . </span></h1>
    </div>

    <?php
    require_once('../includes/lista_produtos.php');

  }
  ?>

</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->

<?php require_once("../includes/rodape.php") ?>

</body>



</html>
