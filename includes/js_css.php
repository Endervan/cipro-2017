
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/popper.min.js"></script>

<script src="<?php echo Util::caminho_projeto(); ?>/js/ie-emulation-modes-warning.js"></script>


<script src="<?php echo Util::caminho_projeto(); ?>/js/ie-emulation-modes-warning.js"></script>

<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/css/non-responsive.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/ie10-viewport-bug-workaround.js"></script>


<script src="<?php echo Util::caminho_projeto() ?>/js/ie10-viewport-bug-workaround.js"></script>




<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<!-- <script type="text/javascript" src="<?php// echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script> -->

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>



<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>

<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
  window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>





<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/js/jquery.flexslider-min.js"></script>
<!-- Syntax Highlighter -->
<!-- <script type="text/javascript" src="<?php //echo Util::caminho_projeto() ?>/js/shCore.js"></script> -->
<!-- <script type="text/javascript" src="<?php //echo Util::caminho_projeto() ?>/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php //echo Util::caminho_projeto() ?>/js/shBrushJScript.js"></script> -->
<script src="<?php echo Util::caminho_projeto() ?>/js/demo.js"></script>

<!-- Optional FlexSlider Additions -->
<!-- <script src="<?php //echo Util::caminho_projeto() ?>/js/jquery.easing.js"></script> -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.js"></script>

<script defer src="<?php echo Util::caminho_projeto() ?>/js/block-slider.min.js"></script>





<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/flexslider.css" type="text/css" media="screen" />




<!-- - LAYER SLIDER - -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/touchcarousel.css"/>
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/black-and-white-skin.css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.touchcarousel-1.2.min.js"></script>

<!-- XXXX LAYER SLIDER XXXX -->




<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE   -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/fonts/open-iconic/font/css/open-iconic-bootstrap.min.css">



<!-- ======================================================================= -->
<!-- USA SETA UP  USE RODAPE -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(document).ready(function(){

  $(window).scroll(function(){
    if ($(this).scrollTop() > 800) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });

  $('.scrollup').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 800);
    return false;
  });

  $('.mapa').click(function(){
    $(".mapa").animate({ scrollTop: 0 }, 800);
    return false;
  });

});
</script>

<!--jQuery GEREALMENTE JA ESTA HEAD VERIFICAR-->

<!-- ======================================================================= -->
<!-- USA SETA UP   -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-select.min.js"></script>


<script type="text/javascript">
$('.selectpicker').selectpicker({
  style: 'btn-info',
  size: 8
});

</script>
<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->



<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->

<!-- the mousewheel plugin - optional to provide mousewheel support -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.min.js"></script>

<!-- the jScrollPane script -->
<link type="text/css" href="<?php echo Util::caminho_projeto() ?>/css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" >
// $(function() {
//   $('.lista_produtos .list-group').jScrollPane();
// });

$(function() {
  $('.scrol_empresa').jScrollPane();
  $('.scrol_empresa01').jScrollPane();
});
</script>
<!-- ======================================================================= -->
<!-- scroll personalizado    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->

<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/js/colorbox/example1/colorbox.css" />
<script src="<?php echo Util::caminho_projeto() ?>/js/colorbox/jquery.colorbox-min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/colorbox/i18n/jquery.colorbox-pt-BR.js"></script>

<script>
$(document).ready(function(){
  //Examples of how to assign the Colorbox event to elements
  $(".group1").colorbox({rel:'group1'});
  $(".group2").colorbox({rel:'group2', transition:"fade"});
  $(".group3").colorbox({rel:'group3', transition:"none", width:"75%", height:"75%"});
  $(".group4").colorbox({rel:'group4', slideshow:true, transition:"none", width:"100%", height:"100%"});
  $(".ajax").colorbox();
  $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});
  $(".vimeo").colorbox({iframe:true, innerWidth:500, innerHeight:409});
  $(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
  $(".inline").colorbox({inline:true, width:"50%"});
  $(".callbacks").colorbox({
    onOpen:function(){ alert('onOpen: colorbox is about to open'); },
    onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
    onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
    onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
    onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
  });

  $('.non-retina').colorbox({rel:'group5', transition:'none'})
  $('.retina').colorbox({rel:'group5', transition:'none', retinaImage:true, retinaUrl:true});

  //Example of preserving a JavaScript event for inline calls.
  $("#click").click(function(){
    $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
    return false;
  });
});
</script>
<!-- ======================================================================= -->
<!-- colobox    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:1});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
    );

    $(".dicas_hover").hover(
      function () {
        $(this).stop().animate({opacity:1});
      },
      function () {
        $(this).stop().animate({opacity:0});
      }
      );

});
</script>
<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--select subcategorias   -->
<!-- ======================================================================= -->
<script type="text/javascript">
$(function() {
    $('#id_categoriaproduto').change(function(){
        $('#id_subcategoriaproduto').load('<?php echo Util::caminho_projeto() ?>/includes/carrega_subcategoria_produtos.php?id='+$('#id_categoriaproduto').val());
    });
});
</script>
<!-- ======================================================================= -->
<!--select subcategorias   -->
<!-- ======================================================================= -->


<script type="text/javascript">
  $(document).ready(function(){
      $('#topo_tipos_veiculos').change(function(){
          $('#topo_marca_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_marcas_veiculos.php?id='+$('#topo_tipos_veiculos').val() );
      });
  });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_marca_veiculos').change(function(){
            $('#topo_modelo_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_modelos_veiculos.php?id='+$('#topo_marca_veiculos').val()+'&tipo_veiculo='+$('#topo_tipos_veiculos').val() );
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_modelo_veiculos').change(function(){
            $('#busca_topo_ano').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_ano.php');
        });
    });
</script>

<script src="<?php echo Util::caminho_projeto() ?>/js/pinterest.js"></script>



<!-- ======================================================================= -->
<!--FUNCAO DATEPIKER IDIOMA LOCAL    -->
<!-- ======================================================================= -->
<!-- usando internalinalizacao br -->

<script src="<?php echo Util::caminho_projeto(); ?>/js/jquery-ui/jquery-ui.min.js"></script>
<script src="<?php echo Util::caminho_projeto(); ?>/js/jquery-ui/jquery-ui18n-pt.js"></script>

<!-- // funcao de traduzir por portugues o calendario -->
<script type="text/javascript">
$( function() {

  // calendario usando button com idioma br
    $("#data").datepicker({
        dateFormat: 'dd-mm-yy',
        minDate: 0,
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
    });

    // desabilitando a digitação do input
    $("#data").attr( 'readOnly' , 'true' );
  });



  $( "#datepicker" ).datepicker( $.datepicker.regional[ "br" ] );
  $( "#locale" ).on( "change", function() {
    $( "#datepicker" ).datepicker( "option",
    $.datepicker.regional[ $( this ).val() ] );
  });

</script>
<!-- ======================================================================= -->
<!--FUNCAO DATEPIKER IDIOMA LOCAL    -->
<!-- ======================================================================= -->
<!-- ======================================================================= -->
<!--FUNCAO DATEPIKER IDIOMA LOCAL    -->
<!-- ======================================================================= -->
<!-- usando internalinalizacao br -->
<link href="<?php echo Util::caminho_projeto(); ?>/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo Util::caminho_projeto(); ?>/js/jquery-ui/jquery-ui.theme.min.css" rel="stylesheet">


<!-- ======================================================================= -->
<!--FUNCAO mascaras dados formulario    -->
<!-- ======================================================================= -->
<!--
<script src="<?php //echo Util::caminho_projeto(); ?>/js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
 $("#data").mask("99-99-9999",{completed:function(){alert("insira data valida: "+this.val());}},{placeholder:"dd-mm-yyyy"});
 $("#data").mask("99-99-9999");
 $("#phone").mask("(99) 99999999?9");
 $("#servico").mask("9?9");
 $("#produto").mask("9?9");
 $.mask.definitions['*']='[AM]';
 $("#hora1").mask("99:99*aa");
});
</script> -->
<!-- ======================================================================= -->
<!--FUNCAO mascaras dados formulario    -->
<!-- ======================================================================= -->

<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap.min.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>


<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<link href="<?php echo Util::caminho_projeto() ;?>/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ;?>/js/moment-with-locales.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ;?>/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$('#hora').datetimepicker({
  format: 'LT',
  format: 'HH:mm'
});

</script>
