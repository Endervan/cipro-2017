
<div class="container-fluid rodape top100">
	<div class="row pb20">

		<div class="container">
			<div class="row top15 bottom10">

				<div class="col-9 padding0 menu_rodape ">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "tratamentos" or Url::getURL( 0 ) == "tratamento" or Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/tratamentos">TRATAMENTOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "clinicas" or Url::getURL( 0 ) == "clinica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/clinicas">CLÍNICAS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
							</a>
						</li>

					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>


				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-3 text-right redes">
					<div class="col">

						<?php if ($config[google_plus] != "") { ?>
							<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
								<i class="fa fa-google-plus-official top20 fa-2x right25"></i>
							</a>
						<?php } ?>


						<?php if ($config[facebook] != "") { ?>
							<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
								<i class="fa fa-facebook-official fa-2x top20 right25"></i>
							</a>
						<?php } ?>

						<?php if ($config[instagram] != "") { ?>
							<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
								<i class="fa fa-instagram fa-2x top20 right25"></i>
							</a>
						<?php } ?>


					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->


				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->



				<div class="col-8 top55 endereco_rodape">
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="row">
						<div class="col-4 media">
							<img class="d-flex mr-2" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_rodape.png" alt="">
							<div class="media-body align-self-center">
								<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
							</div>
						</div>
						<?php if (!empty($config[telefone2])): ?>
							<div class="col media">
								<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_rodape.png" alt="">
								<div class="media-body align-self-center">
									<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
								</div>
							</div>
						<?php endif; ?>



						<!-- ======================================================================= -->
						<!-- ONDE ESTAMOS   -->
						<!-- ======================================================================= -->
						<div class="col-12 top5">
							<div class="media top15">
								<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_endereco_rodape.png" alt="">
								<div class="media-body">
									<p class="mt-0"><?php Util::imprime($config[endereco]); ?></p>
								</div>
							</div>

						</div>
						<!-- ======================================================================= -->
						<!-- ONDE ESTAMOS   -->
						<!-- ======================================================================= -->


					</div>

					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>








				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->
				<div class="col-1 redes top40">
					<div class="row">
						<div class="col">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!--homeweb   -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright CIPRO</h5>
		</div>
	</div>
</div>
