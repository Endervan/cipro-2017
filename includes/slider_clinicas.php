
<style media="screen">
.carroucel_clientes .carousel-indicators {
  position: absolute;
  right: 0;
  bottom: -34px;
  margin: 0;
}

.carroucel_clientes .controle .fa{
  color: #001d33;
}


.carroucel_clientes .carousel-indicators li {
  width: 23px;
  height: 23px;
  background-color: #9494ab;
}


.carroucel_clientes .carousel-indicators li.active {
  cursor: default;
  background: #2a2a57;
}

.carroucel_clientes .carousel-indicators li:hover {
  cursor: pointer;
  background: #2a2a57;
}

.carroucel_clientes .carousel-control-next,
.carroucel_clientes .carousel-control-prev {
    top: 472px;
    width: 70%;
    color: #2a2a57;
    opacity: .5;
    z-index: 99;
}

.group4  img:hover {
	cursor: -webkit-zoom-in;
}

</style>
<?php
$qtd_itens_linha = 1; // quantidade de itens por linha
$id_slider = "carouselExampleIndicators";
unset($linha);
?>


<div id="<?php echo $id_slider ?>" class="carroucel_clientes padding0 top20 carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    $result = $obj_site->select("tb_banners", "AND tipo_banner = 3 limit 6");
    $linhas = mysql_num_rows($result) / $qtd_itens_linha;
    $b_temp = 0;

    for($i_temp = 0; $i_temp < $linhas; $i_temp++){
      ?>
      <li class="rounded-circle" data-target="#<?php echo $id_slider ?>" data-slide-to="<?php echo $i_temp; ?>" class="<?php if($i_temp == 0){ echo "active"; } ?>"></li>
      <?php


      // guarda a qtd de linhas
      $result1 = $obj_site->select("tb_banners", "AND tipo_banner = 3 limit 6", "limit $b_temp, $qtd_itens_linha");

      if(mysql_num_rows($result1) > 0){
        while($row1 = mysql_fetch_array($result1)){
          $linha[] = $row1;
        }
      }

      //  aumenta a qtd de itens a busca na consulta
      $b_temp = $b_temp + $qtd_itens_linha;
    }

    ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">

    <?php
    if (count($linhas) > 0) {
      $i_temp = 0;
      $b_temp = $qtd_itens_linha;
      $c_temp = 0;
      for($i_temp = 0; $i_temp < $linhas; $i_temp++){
        ?>
        <div class="carousel-item <?php if($i_temp == 0){ echo "active"; } ?>">

          <?php

          //  lista os itens da linha
          for($c_temp; $c_temp <$b_temp; $c_temp++){
            $imagem = $linha[$c_temp][imagem];

            if(!empty($linha[$c_temp][titulo])):
              ?>



              <div class="col-12 padding0">


                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $imagem; ?>" class="group2">
                    <?php $obj_site->redimensiona_imagem("../uploads/$imagem", 574, 465, array("class"=>"d-block w-100", "alt"=>"$linha[$c_temp][titulo]") ) ?>
                  </a>


              </div>



              <?php /*
              <div>
              <h1><?php Util::imprime($linha[$c_temp][titulo]) ?></h1>
              </div>
              <!-- <div class="col-xs-8 dicas_descricao_home top20 padding0">
              <p><?php Util::imprime($linha[$c_temp][descricao], 300) ?></p>
              </div>
              */ ?>

              <?php
            endif;
          }
          ?>

        </div>
        <?php
        $b_temp = $b_temp + $qtd_itens_linha;
      }
    }
    ?>

  </div>



  <a class="carousel-control-prev controle" href="#<?php echo $id_slider ?>" role="button" data-slide="prev">
    <i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>
    <span class="sr-only">prev</span>
  </a>
  <a class="carousel-control-next controle" href="#<?php echo $id_slider ?>" role="button" data-slide="next">
    <i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>
    <span class="sr-only">Next</span>
  </a>
</div>
