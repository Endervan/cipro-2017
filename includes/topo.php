<div class="container-fluid border_topo">  <div class="row"></div></div>

<div class="container-fluid ">
  <div class="row">

    <div class="bg_topo">  </div>

    <div class="container ">
      <div class="row">
        <?php
        if(empty($voltar_para)){
          $link_topo = Util::caminho_projeto()."/";
        }else{
          $link_topo = Util::caminho_projeto()."/".$voltar_para;
        }
        ?>

        <div class="col-3 top5 logo">
          <a href="<?php echo Util::caminho_projeto() ?>/" title="início">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="início" class="">
          </a>
        </div>

        <div class="col-9">

          <div class=" top5">
            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->
            <div class="telefone_topo">
              <div class="row">

                <div class="col-3 media ml-auto">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                  </div>
                </div>
                <?php if (!empty($config[telefone2])): ?>
                  <div class="col-3 media">
                    <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                    </div>
                  </div>
                <?php endif; ?>


                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->

                <div class="col-4 avaliacao_topo  carrinho">
                  <!-- Large modal -->
                  <a type="a" class="btn btn_topo" data-toggle="modal" data-target=".bd-example-modal-lg">
                    <img src="<?php echo Util::caminho_projeto(); ?>/imgs/agendar.png" alt="" />

                  </a>

                  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <h5 class="modal-title" id="myLargeModalLabel">SELECIONE TRATAMENTO DESEJADO</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>
                        <div class="modal-body modal-geral">

                          <!-- ======================================================================= -->
                          <!-- MENU LATERAL  -->
                          <!-- ======================================================================= -->
                          <div class="col-12 menu_lateral_home">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">


                              <?php
                              $i=0;
                              $result = $obj_site->select("tb_categorias_produtos");
                              if (mysql_num_rows($result) > 0) {
                                while($row = mysql_fetch_array($result)){

                                  ?>

                                  <div class="row panel panel-default">


                                    <div class="col-12 panel-heading" role="tab" id="heading<?php echo $i; ?>">
                                      <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                        <?php Util::imprime($row[titulo]); ?> <i class="fa fa-angle-down fa-2x pull-right"></i>
                                      </a>
                                    </div>


                                    <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse col-12" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
                                      <div class="list-group">

                                        <?php
                                        $result1 = $obj_site->select("tb_produtos", "and id_categoriaproduto = $row[0] ");
                                        if (mysql_num_rows($result1) > 0) {
                                          while($row1 = mysql_fetch_array($result1)){
                                            ?>
                                            <div class="row">

                                              <div class="col-12">
                                                <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>/<?php Util::imprime($row1[url_amigavel]); ?>" class="col-8">
                                                  <?php Util::imprime($row1[titulo]); ?>
                                                </a>
                                                <a href="javascript:void(0);" class="btn btn_agendar col-3 pull-right" title="AGENDA" onclick="add_solicitacao(<?php Util::imprime($row1[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row1[0]) ?>, 'produto'">
                                                  AGENDA
                                                </a>
                                              </div>
                                            </div>
                                            <?php
                                          }
                                        }
                                        ?>



                                      </div>
                                    </div>



                                  </div>


                                  <?php
                                  $i++;
                                }
                              }
                              ?>




                            </div>
                          </div>
                          <!-- ======================================================================= -->
                          <!-- MENU LATERAL  -->
                          <!-- ======================================================================= -->

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <!--  ==============================================================  -->
                <!--CARRINHO-->
                <!--  ==============================================================  -->



              </div>
            </div>

            <!-- ======================================================================= -->
            <!-- telefones  -->
            <!-- ======================================================================= -->


            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->
            <div class="col-12 top30 menu">
              <!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
              <div id="navbar">
                <ul class="nav navbar-nav navbar-right align-self-center">
                  <li >
                    <a class=" top10 <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/">HOME
                    </a>
                  </li>

                  <li >
                    <a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
                    </a>
                  </li>



                  <li>
                    <a class="<?php if(Url::getURL( 0 ) == "tratamentos" or Url::getURL( 0 ) == "tratamento" or Url::getURL( 0 ) == "orcamento" or Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/tratamentos">TRATAMENTOS
                    </a>
                  </li>


                  <li>
                    <a class="<?php if(Url::getURL( 0 ) == "clinicas" or Url::getURL( 0 ) == "clinica"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/clinicas">CLÍNICAS
                    </a>
                  </li>

                  <li >
                    <a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
                    </a>
                  </li>

                  <li >
                    <a class="<?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
                      href="<?php echo Util::caminho_projeto() ?>/fale-conosco">FALE CONOSCO
                    </a>
                  </li>

                </ul>
              </div><!--/.nav-collapse -->

            </div>
            <!--  ==============================================================  -->
            <!-- MENU-->
            <!--  ==============================================================  -->

          </div>



        </div>

      </div>
    </div>


  </div>
</div>
