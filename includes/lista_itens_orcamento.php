<div class="col-12 bg_branco01 padding0 tabela_carrinho ">
  <table class="table  table-condensed">

    <tbody>
      <div class="pt20 left15">  <h3 class=""><span>TRATAMENTOS DESEJADOS <?php //echo count($_SESSION[solicitacoes_produtos])+count($_SESSION[solicitacoes_servicos]); ?></span></h3></div>


      <?php
      if(count($_SESSION[solicitacoes_produtos]) > 0){
        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
          $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
          ?>
          <tr>
            <td>
              <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 98, 65, array("class"=>"img-rounded", "alt"=>"$row[titulo]")) ?>
            </td>
            <td class="col-12 text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
            <?php /*
            <td class="text-center">
              <!-- QTD.<br> -->
              <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
              <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
              <div class="help-block with-errors"></div>
            </td>
            */ ?>
            <td class="text-center">
              <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                <i class="fa fa-times-circle fa-2x"></i>
              </a>
            </td>
          </tr>
          <?php
        }
      }
      ?>

      <?php /*
      if(count($_SESSION[solicitacoes_servicos]) > 0){
        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
          $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
          ?>
          <tr>
            <td>
              <!-- <img width="60" height="60"class="media-object" src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" alt="" /> -->
              <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 98, 65, array("class"=>"img-rounded", "alt"=>"$row[titulo]")) ?>
            </td>
            <td class="col-12 text-uppercase"><?php Util::imprime($row[titulo]); ?></td>
            <!-- <td class="text-center">
              QTD.<br>
              <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
              <input name="idservico[]" type="hidden" value="<?php //echo $row[0]; ?>"  />
            </td> -->
            <td class="text-center">
              <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                <i class="fa fa-times-circle fa-2x"></i>
              </a>
            </td>
          </tr>
          <?php
        }
      }
      */?>

    </table>
  </div>
  <!-- ======================================================================= -->
  <!-- CARRINHO  -->
  <!-- ======================================================================= -->
