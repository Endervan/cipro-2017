<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 11);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 9);?>
      <div class="col-6 top100 bottom150">
        <h4><span><?php Util::imprime($row[titulo]); ?></span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   clinica DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container top50">
    <div class="row empresa_detalhe">

      <div class="col-6">
        <!-- ======================================================================= -->
        <!-- slider clinicas    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/slider_clinicas.php') ?>
        <!-- ======================================================================= -->
        <!-- slider clinicas    -->
        <!-- ======================================================================= -->
      </div>

      <div class="col-6 top15">
        <p><span><?php Util::imprime($row[descricao]); ?></span></p>
      </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa",6);?>
      <div class="col-12 top50">
        <div class="media top40">
          <img class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_clinicas01.png" alt="">
          <div class="media-body">
            <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
      <div class="col-12 ">
        <div class="media top40">
          <img class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_clinicas02.png" alt="">
          <div class="media-body">
            <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   clinica DESCRICAO -->
  <!--  ==============================================================  -->


<?php /*

    <div class="container top50">
      <div class="row ">
        <div class="col-12 top50">
          <h4>NOSSOS PARCEIROS</h4>
          <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
        </div>

        <div class="col-12 padding0">
          <!-- ======================================================================= -->
          <!-- slider clinicas    -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/slider_clientes.php') ?>
          <!-- ======================================================================= -->
          <!-- slider clinicas    -->
          <!-- ======================================================================= -->
        </div>



      </div>
    </div>

*/ ?>

  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->
  <div class="container top35">
    <div class="row clinica_home">
      <div class="col-12 text-center top50 bottom30">
        <h4>ENCONTRE NOSSA CLÍNICA</h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
      <div class="col-6">
        <p><?php Util::imprime($config[endereco]); ?></p>
      </div>

      <div class="mr-auto col-4">
        <div class="row">
            <p><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></p>
            <p><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></p>
        </div>
      </div>
      <div class="col-12 top25 mapa">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
