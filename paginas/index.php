<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">
      <div id="container_banner">
        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");
            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->





  <!--  ==============================================================  -->
  <!--   CATEGORIAS LISTAGEM PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container top60 bottom75">
    <div class="row cat_produtos">

      <div class="col-12 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
        <div class="desc_empresa">
          <h4><?php Util::imprime($row[titulo]); ?></h4>
          <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
        </div>
        <div class="col-10 col-offset-1 top30 ">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>

      <?php
      $result = $obj_site->select("tb_categorias_produtos","and exibir_home = 'SIM' limit 4");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          $produtos[] = $row;
          ?>
          <div class="col-3 top30 ">
            <div class="card text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",260, 288, array("class"=>"text-center imagem", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body">
                <div class="text-uppercase line_text_prod"><h3 class="card-title text-uppercase"><?php Util::imprime($row[titulo],40); ?></h3></div>
                <img  src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem_icon]); ?>" alt="" />

              </div>

              <div class="text-left top40">
                <?php
                $result1 = $obj_site->select("tb_produtos", "and id_categoriaproduto = $row[0] limit 4");
                if (mysql_num_rows($result1) > 0) {
                  while($row1 = mysql_fetch_array($result1)){
                    ?>
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row1[url_amigavel]); ?>">
                      <h2 class="top20"><i class="fa fa-star right10"></i><?php Util::imprime($row1[titulo]); ?></h2>
                    </a>

                    <?php
                  }
                }
                ?>

                <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
              </div>

            </div>
          </div>

          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   CATEGORIAS LISTAGEM PRODUTOS -->
  <!--  ==============================================================  -->





  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_empresa_home">
    <div class="row">

      <div class="container">
        <div class="row top100">
          <div class="col-12 text-right">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
            <div class="desc_empresa">
              <h4><?php Util::imprime($row[titulo]); ?></h4>
            </div>
          </div>

          <div class="col-7 ml-auto top60 scrol_empresa">
            <p><span><?php Util::imprime($row[descricao]); ?></span></p>
          </div>

          <div class="col-7 ml-auto top20 bottom40">
            <a class="btn btn_saiba_empresa" href="<?php echo Util::caminho_projeto() ?>/empresa" title="MAIS DETALHES">
              SAIBA MAIS
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row clinica_home">
      <div class="col-12 text-center top25 bottom40">
        <h4>ENCONTRE NOSSA CLÍNICA</h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
      <div class="col-6">
        <p><?php Util::imprime($config[endereco]); ?></p>
      </div>

      <div class="mr-auto col-4">
        <div class="row">
          <h2><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h2>
          <h2><span><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span></h2>
        </div>
      </div>
      <div class="col-12 mapa top20">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->




  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->
  <div class="container bottom100">
    <div class="row dicas_home">
      <div class="col-12 top30">
        <h4>NOSSAS DICAS</h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>


      <?php
      $result = $obj_site->select("tb_dicas"," ORDER BY RAND() limit 3");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
          $i=0;
          ?>

          <div class="col-4 top30">
            <div class="card">
              <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 340, 218, array("class"=>"card-img-top p-3", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body">
                <div>
                  <h2 class="text-uppercase desc_dicas_home"><?php Util::imprime($row[titulo]); ?></h2>
                </div>
                <div class="dicas_home_descricao top5">
                  <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
                </div>

              </div>

              <div class="dicas_hover">
                <div class="col-12 text-center">
                  <a href="<?php echo Util::caminho_projeto() ;?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="SAIBA MAIS">
                    <img src="<?php echo Util::caminho_projeto() ;?>/imgs/btn-saiba-mais.png" alt="" class="">
                  </a>
                </div>
              </div>

            </div>
          </div>

          <?php
          if ($i==2) {
            echo "<div class='clearfix'>  </div>";
          }else {
            $i++;
          }
        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true,
      delay: 6000
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>
