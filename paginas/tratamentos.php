<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'tratamentos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top115 bottom150">
        <h4><span>NOSSOS TRATAMENTOS</span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">


      <div class="col-12 text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
        <div class="top30 ">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>
      </div>



      <div class="col-12 top15 padding0">
        <div class="row cat_produtos_geral">

          <?php

          //  busca os produtos sem filtro
          $result = $obj_site->select("tb_categorias_produtos", $complemento);

          if(mysql_num_rows($result) == 0){
            echo "
            <div class=' col-12 text-center '>
            <h2 class='btn_nao_encontrado' style='padding: 100px;'>Nenhum produto encontrado.</h2>
            </div>"
            ;
          }else{
            ?>
            <!-- <div class="col-12 total-resultado-busca">
              <h2><span><?php //echo mysql_num_rows($result) ?></span> TRATAMENTO(S) ENCONTRADO(S) . </h2>
            </div> -->

            <?php
            require_once('./includes/lista_tratamentos.php');

          }
          ?>

        </div>
      </div>
    </div>
  </div>

  <!-- ======================================================================= -->
  <!-- PRODUTOS    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
