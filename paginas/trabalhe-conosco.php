<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",10) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->





  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-12 top80 bottom45">
        <h4><span>TRABALHE CONOSCO</span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <div class="container relativo">
    <div class="row ">

      <div class="col-4">


        <!-- ======================================================================= -->
        <!-- telefones  -->
        <!-- ======================================================================= -->
        <div class="col-12">
          <div class="media">
            <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_topo.png" alt="">
            <div class="media-body align-self-center">
              <h5 class="mt-0 ml-3"><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h5>
            </div>
          </div>
        </div>


        <?php if (!empty($config[telefone2])): ?>
          <div class="col-12">
            <div class="top10 media">
              <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_topo.png" alt="">
              <div class="media-body align-self-center">
                <h5 class="mt-0 ml-3"><span><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span></h5>
              </div>
            </div>
          </div>
        <?php endif; ?>
        <!-- ======================================================================= -->
        <!-- telefones  -->
        <!-- ======================================================================= -->


        <!-- ======================================================================= -->
        <!-- links  -->
        <!-- ======================================================================= -->
        <ul class="nav nav-pills text-center flex-column top20">
          <li class="nav-item  col-12">
            <a class="nav-link"  href="<?php echo Util::caminho_projeto() ?>/fale-conosco" title="fale conosco">
              <div class="media">
                <img class="d-flex align-self-center" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_fale.png" alt="">
                <div class="media-body">
                  <span class="clearfix mt-0">TIRE SUAS DÚVIDAS</span>FALE CONOSCO
                </div>
              </div>
            </a>
          </li>
          <li class="nav-item  col-12 top10">
            <a class="nav-link active" disabled href="#" title="">
              <div class="media">
                <img class="d-flex align-self-center" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_trabalhe.png" alt="">
                <div class="media-body">
                  <span class="clearfix mt-0">FAÇA PARTE DA NOSSA EQUIPE</span>TRABALHE CONOSCO
                </div>
              </div>
            </a>
          </li>

        </ul>
        <!-- ======================================================================= -->
        <!-- links  -->
        <!-- ======================================================================= -->

      </div>

      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->
      <div class="col-8 fundo_formulario">

        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">

          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="escolaridade" class="form-control fundo-form form-control-lg input-lg" placeholder="ESCOLARIDADE">
                <span class="fa fa-graduation-cap form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="area" class="form-control fundo-form form-control-lg input-lg" placeholder="ÁREA">
                <span class="fa fa-briefcase form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="cargo" class="form-control fundo-form form-control-lg input-lg" placeholder="CARGO">
                <span class="fa fa-address-book form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="cidade" class="form-control fundo-form form-control-lg input-lg" placeholder="CIDADE">
                <span class="fa fa-home form-control-feedback"></span>
              </div>
            </div>

            <div class="col">
              <div class="form-group  icon_form">
                <input type="text" name="estado" class="form-control fundo-form form-control-lg input-lg" placeholder="ESTADO">
                <span class="fa fa-globe form-control-feedback"></span>
              </div>
            </div>
          </div>

          <div class="form-row">

            <div class="col">
              <div class="form-group  icon_form">
                <input type="file" name="curriculo" class="form-control fundo-form form-control-lg input-lg" placeholder="CURRICULO">
                <span class="fa fa-file-text form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="form-row">
            <div class="col">
              <div class="form-group icon_form">
                <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                <span class="fa fa-pencil form-control-feedback"></span>
              </div>
            </div>
          </div>


          <div class="col-12 text-center">
            <button type="submit" class="btn btn_formulario" name="btn_contato">
              ENVIAR
            </button>
          </div>


        </div>
      </form>


      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->

    </div>
  </form>


</div>


<!--  ==============================================================  -->
<!--   NOSSA CLINICA -->
<!--  ==============================================================  -->
<div class="container top35">
  <div class="row clinica_home">
    <div class="col-12 text-center top50 bottom50">
      <h4>ENCONTRE NOSSA CLÍNICA</h4>
      <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
    </div>
    <div class="col-6">
      <p><?php Util::imprime($config[endereco]); ?></p>
    </div>

    <div class="mr-auto col-4">
      <div class="row">
        <h2><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h2>
        <h2><span><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span></h2>
      </div>
    </div>
    <div class="col-12 mapa">
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>

  </div>
</div>
<!--  ==============================================================  -->
<!--   NOSSA CLINICA -->
<!--  ==============================================================  -->





<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 270,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{

  if(!empty($_FILES[curriculo][name])):
    $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
    $texto = "Anexo: ";
    $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
    $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
  endif;

  $texto_mensagem = "
  Nome: ".$_POST[nome]." <br />
  Email: ".$_POST[email]." <br />
  Telefone: ".$_POST[telefone]." <br />
  Escolaridade: ".$_POST[escolaridade]." <br />
  Area Pretentida: ".$_POST[area]." <br />
  Cargo: ".$_POST[cargo]." <br />
  Cidade: ".$_POST[cidade]." <br />
  Estado: ".$_POST[estado]." <br />


  Mensagem: <br />
  ".nl2br($_POST[mensagem])."

  <br><br>
  $texto
  ";


  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
  Util::alert_bootstrap("Obrigado por entrar em contato.");
  unset($_POST);
}

?>




<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Informe nome.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Seu email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu numero.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione sua Cidade.'
          }
        }
      },
      estado: {
        validators: {
          notEmpty: {
            message: 'Por favor adicione seu Estado.'
          }
        }
      },
      escolaridade1: {
        validators: {
          notEmpty: {
            message: 'Sua Informação Acadêmica.'
          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
            maxSize: 5*1024*1024,   // 5 MB
            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      }
    }
  });
});
</script>


<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript">

$('#hora').datetimepicker({
  format: 'LT'
});

</script>
