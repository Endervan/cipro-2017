<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container produto_cat_destaque">
    <div class="row ">
      <div class="col-12 top100 bottom100">
        <h4>
          <span><?php Util::imprime($dados_dentro[titulo]); ?></span>
        </h4>

        <h4>
          <span>
            <b><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo")); ?></b>
          </span>
        </h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container top60 bottom60">
    <div class="row produto_dentro">

      <div class="col-6 order-12">
        <div class="top30">
          <h5 class="text-uppercase"><?php Util::imprime($dados_dentro[titulo]); ?></h5>
        </div>
        <div class="top40"><h1>AGENDE UMA AVALIAÇÃO</h1></div>

        <div class="row">

          <div class="col-6">
            <a href="javascript:void(0);" class="btn btn_produtos_orcamento" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
              AGENDAR AGORA
            </a>
          </div>
          <div class="col-6 padding0 top20">
            <h6>OU LIGUE</h6>
            <h6><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h6>
          </div>
        </div>


        <div class="top40">
          <h2><?php Util::imprime($dados_dentro[subtitulo]); ?></h2>
        </div>


      </div>


      <div class="col-6 padding0 order-1">

        <!-- ======================================================================= -->
        <!-- SLIDER CATEGORIA -->
        <!-- ======================================================================= -->
        <div class="col-12  slider_prod_geral">


          <!-- Place somewhere in the <body> of your page -->
          <div id="slider" class="flexslider">

            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) == 0){
              echo "<div class='imagem_default'><img src='../imgs/default.png' alt=''></div>";
            }else{
              ?>

              <div class="zomm">  </div>


              <!-- <span>Clique na imagem para Ampliar</span> -->
              <ul class="slides slider-prod">

                <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li class="zoom">
                      <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 566, 406, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                        <!-- <img src="<?php //echo Util::caminho_projeto() ?>/uploads/<?php //Util::imprime($row[imagem]); ?>" class="input100" /> -->
                      </a>
                    </li>
                    <?php
                  }
                }
                ?>
                <!-- items mirrored twice, total of 12 -->
              </ul>
            </div>
            <div id="carousel" class="flexslider">
              <ul class="slides slider-prod-tumb">
                <?php
                $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
                if(mysql_num_rows($result) > 0)
                {
                  while($row = mysql_fetch_array($result)){
                    ?>
                    <li>
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />
                    </li>
                    <?php
                  }
                }

              }
              ?>
              <!-- items mirrored twice, total of 12 -->
            </ul>
          </div>

        </div>




      </div>



    </div>
    <div class="row">
      <div class="col-12">
        <div class="desc_geral_produto top20">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
      </div>
    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DESCRICAO -->
  <!--  ==============================================================  -->






  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <div class="container top70">
    <div class="row cat_produtos_geral bg_cinza">

      <div class="col-12 top25">
        <h5>OUTROS TRATAMENTOS</h5>
      </div>

      <?php
      $i = 0;
      $result = $obj_site->select("tb_produtos", "order by rand() limit 3");
      if(mysql_num_rows($result) == 0){
        echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
      }else{
        while ($row = mysql_fetch_array($result))
        {
          $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
          $row_categoria = mysql_fetch_array($result_categoria);
          ?>
          <div class="col-4">
            <div class="top35 bottom20">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="bottom" title="Saiba Mais" >
                <h2 class="text-uppercase top15"><i class="fa fa-star right10"></i><?php Util::imprime($row[titulo]); ?></h2>
                <div class="top10 produt_cat"><p><?php Util::imprime($row[descricao],140); ?></p></div>
              </a>
            </div>
          </div>

          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>





<script type="text/javascript">
$(window).load(function() {


  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: true,
    slideshow: false,
    sync: "#carousel"
  });


});
</script>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
