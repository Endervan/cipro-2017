<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top115 bottom150">
        <h4><span>DICAS</span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



    <!--  ==============================================================  -->
    <!--   DICAS -->
    <!--  ==============================================================  -->
    <div class="container bottom60">
      <div class="row dicas_home">


        <?php
        $result = $obj_site->select("tb_dicas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            ?>

            <div class="col-4 top40">
              <div class="card">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 340, 218, array("class"=>"card-img-top p-3", "alt"=>"$row[titulo]")) ?>
                </a>
                <div class="card-body">
                  <div>
                    <h2 class="desc_dicas_home text-uppercase"><?php Util::imprime($row[titulo]); ?></h2>
                  </div>
                  <div class="dicas_home_descricao">
                    <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
                  </div>

                </div>

                <div class="dicas_hover">
                  <div class="col-12 text-center">
                    <a href="<?php echo Util::caminho_projeto() ;?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" data-toggle="tooltip" data-placement="top" title="SAIBA MAIS">
                      <img src="<?php echo Util::caminho_projeto() ;?>/imgs/btn-saiba-mais.png" alt="" class="">
                    </a>
                  </div>
                </div>

              </div>
            </div>

            <?php
            if ($i==2) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   DICAS -->
    <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
