<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <div class="col-6 top115 bottom150">
        <h4><span><?php Util::imprime($row[titulo]); ?></span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->


  <!--  ==============================================================  -->
  <!--   EMPRESA DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container top20">
    <div class="row empresa_detalhe">
      <div class="col-12">
        <p><span><?php Util::imprime($row[descricao]); ?></span></p>
      </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
      <div class="col-12">
        <div class="media top40">
          <img class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_empresa02.png" alt="">
          <div class="media-body">
            <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
      <div class="col-12">
        <div class="media top40">
          <img class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_empresa01.png" alt="">
          <div class="media-body">
            <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>

      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
      <div class="col-12">
        <div class="media top40">
          <img class="d-flex align-self-center mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_empresa03.png" alt="">
          <div class="media-body">
            <h5 class="mt-0 bottom20"><?php Util::imprime($row[titulo]); ?></h5>
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA DESCRICAO -->
  <!--  ==============================================================  -->




  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row clinica_home">
      <div class="col-12 text-center top50 bottom50">
        <h4>ENCONTRE NOSSA CLÍNICA</h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
      <div class="col-6">
        <p><?php Util::imprime($config[endereco]); ?></p>
      </div>

      <div class="mr-auto col-4">
        <div class="row">
            <h2><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h2>
            <h2><span><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span></h2>
        </div>
      </div>
      <div class="col-12 bottom60 top30 mapa">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
