<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>





<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6 top100 bottom50">
        <h4><span>AGENDAR UM AVALIAÇÃO</span></h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <div class="container relativo">
    <form class="FormContatos was-validated" role="form" method="post" enctype="multipart/form-data">
      <div class="row ">

        <div class="col-4">
          <!-- ======================================================================= -->
          <!-- CARRINHO  -->
          <!-- ======================================================================= -->
          <?php require_once('./includes/lista_itens_orcamento.php') ?>
          <!-- ======================================================================= -->
          <!-- FORMULARIO  -->
          <!-- ======================================================================= -->


        </div>


        <div class="col-8">
          <div class="fundo_formulario">
            <!-- <h3 class="top20">CONFIRME SEUS DADOS</h3> -->


            <div class="form-row">
              <div class="form-group  col icon_form">
                <input type="text" name="nome" class="form-control fundo-form form-control-lg" placeholder="NOME"  required>
                <span class="fa fa-user form-control-feedback"></span>
              </div>
              <div class="form-group col icon_form">
                <input type="email" name="email" class="form-control fundo-form form-control-lg"  placeholder="EMAIL" required>
                <span class="fa fa-envelope form-control-feedback"></span>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col icon_form">
                <input type="tel" id="phone" title='preenchar Telefone válido(fixo ou celular)' name="telefone" class="form-control fundo-form form-control-lg" placeholder="TELEFONE"  required>
                <span class="fa fa-phone form-control-feedback"></span>
              </div>
              <div class="form-group col icon_form">
                <input type="text" name="localidade" class="form-control fundo-form form-control-lg"  placeholder="LOCALIDADE" required>
                <span class="fa fa-home form-control-feedback"></span>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col icon_form">
                <input type="text" id="data" name="data" class="form-control fundo-form form-control-lg" placeholder="Data"  required>
                <span class="fa fa-calendar form-control-feedback"></span>
              </div>
              <div class="form-group col icon_form">
                <select name="hora" id="hora" class=" form-control fundo-form form-control-lg custom-select" required>
                    <option value="">Selecione a data</option>
                </select>
                <span class="fa fa-clock-o  form-control-feedback  aria-hidden="true""></span>
              </div>
            </div>

            <div class="form-row">
              <div class="col">
                <div class="form-group icon_form">
                  <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="OBSERVAÇÕES"></textarea>
                  <span class="fa fa-pencil form-control-feedback"></span>
                </div>
              </div>
            </div>


            <div class="col-12 text-center  top15 padding0">
              <button type="submit" class="btn btn_formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>

          </div>

        </div>
        <!--  ==============================================================  -->
        <!-- FORMULARIO-->
        <!--  ==============================================================  -->

      </div>
    </form>




  </div>



  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->
  <div class="container top35">
    <div class="row clinica_home">
      <div class="col-12 text-center top50 bottom50">
        <h4>ENCONTRE NOSSA CLÍNICA</h4>
        <img src="<?php echo Util::caminho_projeto() ;?>/imgs/barra_dicas.jpg" alt="">
      </div>
      <div class="col-6">
        <p><?php Util::imprime($config[endereco]); ?></p>
      </div>

      <div class="mr-auto col-4">
        <div class="row">
          <h2><span><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h2>
          <h2><span><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></span></h2>
        </div>
      </div>
      <div class="col-12 mapa">
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
        <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="443" frameborder="0" style="border:0" allowfullscreen></iframe>
        <!-- ======================================================================= -->
        <!-- mapa   -->
        <!-- ======================================================================= -->
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   NOSSA CLINICA -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>

<!-- ======================================================================= -->
<!-- MOMENTS  -->
<!-- ======================================================================= -->
<!-- <link href="<?php //echo Util::caminho_projeto() ;?>/css/bootstrap-datetimepicker-standalone.css" rel="stylesheet">
<script src="<?php //echo Util::caminho_projeto() ;?>/js/moment-with-locales.min.js"></script>
<script src="<?php //echo Util::caminho_projeto() ;?>/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript">
$('#hora').datetimepicker({
  format: 'LT',
  format: 'HH:mm'
});

</script> -->


<!-- busca os horarios de entrega -->
<script type="text/javascript">
$(function() {

    $('#data').change(function(){
        $('#hora').load('<?php echo Util::caminho_projeto(); ?>/horarios_entrega.php?id='+$('#data').val());
    });

});
</script>



<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){



  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
  $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Nome: $_POST[nome] <br />
  Email: $_POST[email] <br />
  Telefone: $_POST[telefone] <br />
  Data da Visita: $_POST[data] <br />
  Hora da visita: $_POST[hora] <br />
  Mensagem: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  </table>

  ";


  Util::envia_email('endvan@gmail.com', utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);


//  Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
//  Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
  unset($_SESSION[solicitacoes_produtos]);
  unset($_SESSION[solicitacoes_servicos]);
  unset($_SESSION[piscinas_vinil]);
  Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>


<
