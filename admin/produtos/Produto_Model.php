<?php
ob_start();
session_start();

class Produto_Model extends Dao
{

	private $nome_tabela = "tb_produtos";
	private $chave_tabela = "idproduto";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
		$this->obj_imagem = new Imagem();
		parent::__construct();

	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

		if(count($ordem) > 0):

			foreach($ordem as $key=>$orde):

				$sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
				parent::executaSQL($sql);

			endforeach;

		endif;

	}



	/*  ==================================================================================================================  */
	/*  VERIFICA SE A CATEGORIA ESTÁ SELECIONADA  */
	/*  ==================================================================================================================  */
	public function verifica_categoria_produto($id_categoriaproduto, $idproduto){
		$sql = "SELECT * FROM tb_produtos_categorias WHERE id_categoriaproduto = '$id_categoriaproduto' and id_produto = '$idproduto'  ";
		$result = parent::executaSQL($sql);

		if (mysql_num_rows($result) > 0) {
			return 'selected';
		}else{
			return '';
		}
	}







	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
		$obj_jquery = new Biblioteca_Jquery();
		$obj_site = new Site();
		?>

		<script type="text/javascript" language="javascript">
		jQuery(function($){
			$(".moeda").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
		});
		</script>



		<script type="text/javascript">
		$(document).ready(function(){
			$('#id_tipoveiculo').change(function(){
				$('#modelos_aceitos').load('carrega_modelos.php?id='+$('#id_tipoveiculo').val()+'&id_produto=<?php echo $_GET[id] ?>');
			});

			$('#modelos_aceitos').load('carrega_modelos.php?id='+$('#id_tipoveiculo').val()+'&id_produto=<?php echo $_GET[id] ?>');
		});
		</script>

		<script type="text/javascript">
		$(function() {
				$('#id_categoriaproduto').change(function(){
						$('#id_subcategoriaproduto').load('<?php echo Util::caminho_projeto() ?>/includes/carrega_subcategoria_produtos.php?id='+$('#id_categoriaproduto').val());
				});
		});
		</script>



		<div class="col-xs-4 form-group pt0">
			<label>Tratamento</label>
			<?php Util::cria_select_bd("tb_categorias_produtos", "idcategoriaproduto", "titulo", "id_categoriaproduto", $dados[id_categoriaproduto], "form-control fundo-form1 input100") ?>
		</div>


<?php /*
		<div class="col-xs-4 form-group pt0">
				<label>Sub-Categoria</label>
				<?php
					$sql = "SELECT * FROM tb_subcategorias_produtos WHERE id_categoriaproduto = '$dados[id_categoriaproduto]'";
				Util::cria_select_bd_com_sql('id_subcategoriaproduto', $dados[id_subcategoriaproduto], $sql, "", "form-control fundo-form1 input100");
				?>
		</div>
		*/ ?>

		<div class="col-xs-2 form-group ">
			<label>Exibir na home</label>
			<?php Util::get_sim_nao("exibir_home", $dados[exibir_home], 'form-control fundo-form1 input100') ?>
		</div>


<?php /*
		<div class="col-xs-2 form-group ">
			<label>Exibir Produto Destaque Home</label>
			<?php Util::get_sim_nao("exibir_home_destaque", $dados[exibir_home_destaque], 'form-control fundo-form1 input100') ?>
		</div>
*/ ?>
		<div class="clearfix">	</div>



		<div class="col-xs-6 form-group ">
			<label>Título</label>
			<input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
		</div>


		<div class="col-xs-12 form-group ">
			<label>Subtitulo</label>
			<input type="text" name="subtitulo" value="<?php Util::imprime($dados[subtitulo]) ?>" class="form-control fundo-form1 input100" >
		</div>




		<div class="col-xs-12 form-group ">
			<label>Imagem da capa</label>
			<?php
			if(!empty($dados[imagem])){
				?>
				<input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-1" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" />
			<?php } else { ?>
				<input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
		</div>


<?php /*
		<div class="col-xs-12 form-group ">
			<label>catalogo</label>
			<?php
			if(!empty($dados[catalogo])){
				?>
				<input type="file" name="catalogo" id="catalogo" value="<?php Util::imprime($dados[catalogo]) ?>" class="form-control fundo-form1 input100" />
				<label>Atual:</label>
				<div class="clearfix"></div>
				<img class="col-xs-1" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[catalogo]) ?>" />
			<?php } else { ?>
				<input type="file" name="catalogo" id="catalogo" value="<?php Util::imprime($dados[catalogo]) ?>" class="form-control fundo-form1 input100" />
			<?php } ?>
		</div>
		*/ ?>


		<div class="col-xs-12 form-group ">
			<label>Descrição</label>
			<?php $obj_jquery->ckeditor('descricao', $dados[descricao]); ?>
		</div>


		<!-- ======================================================================= -->
		<!-- GOOGLE SEO    -->
		<!-- ======================================================================= -->
		<?php require_once("../includes/google_seo.php"); ?>
		<!-- ======================================================================= -->
		<!-- GOOGLE SEO    -->
		<!-- ======================================================================= -->





		<!-- ======================================================================= -->
		<!-- VALIDACAO DO FORMULARIO    -->
		<!-- ======================================================================= -->
		<script>
		$(document).ready(function() {
			$('.FormPrincipal').bootstrapValidator({
				message: 'This value is not valid',
				feedbackIcons: {
					valid: 'fa-ok',
					invalid: 'fa-remove',
					validating: 'fa-refresh'
				},
				fields: {


					titulo: {
						validators: {
							notEmpty: {

							}
						}
					},

					preco: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_categoriaproduto: {
						validators: {
							notEmpty: {

							}
						}
					},


					garantia: {
						validators: {
							notEmpty: {

							}
						}
					},


					indice_velocidade: {
						validators: {
							notEmpty: {

							}
						}
					},

					exibir_home: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_tipoveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},


					id_marcaveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},

					id_modeloveiculo: {
						validators: {
							notEmpty: {

							}
						}
					},

					ano_inicial: {
						validators: {
							notEmpty: {

							}
						}
					},

					ano_final: {
						validators: {
							notEmpty: {

							}
						}
					},

					indice_carga: {
						validators: {
							notEmpty: {

							}
						}
					},


					largura: {
						validators: {
							notEmpty: {

							}
						}
					},


					altura: {
						validators: {
							notEmpty: {

							}
						}
					},

					aro: {
						validators: {
							notEmpty: {

							}
						}
					},


					medida: {
						validators: {
							notEmpty: {

							}
						}
					},


					valor: {
						validators: {
							notEmpty: {

							},
							numeric: {
								transformer: function($field, validatorName, validator) {
									var value = $field.val();
									return value.replace(',', '');
								}
							}
						}
					},


					email: {
						validators: {
							notEmpty: {

							},
							emailAddress: {
								message: 'Esse endereço de email não é válido'
							}
						}
					},

					mensagem: {
						validators: {
							notEmpty: {

							}
						}
					}
				}
			});
		});
		</script>




		<?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");

			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[imagem], 236, 212);
		}

		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[catalogo][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[catalogo] = Util::upload_imagem("../../uploads", $_FILES[catalogo], "4194304");

			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[catalogo], 236, 212);
		}

		//	CADASTRA O USUARIO
		$id = parent::insert($this->nome_tabela, $dados);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


		Util::script_msg("Cadastro efetuado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[imagem][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");


			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[imagem], 236, 212);
		}

		//	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
		if($_FILES[catalogo][name] != "")
		{
			//	EFETUO O UPLOAD DA IMAGEM
			$dados[catalogo] = Util::upload_imagem("../../uploads", $_FILES[catalogo], "4194304");

			//  CRIO O CROP
			$this->efetua_crop_imagem($dados[catalogo], 236, 212);
		}

		parent::update($this->nome_tabela, $id, $dados);


		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



		Util::script_msg("Alterado com sucesso.");
		Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
		if($ativo == "SIM")
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
		else
		{
			$sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
			parent::executaSQL($sql);

			//	ARMAZENA O LOG
			parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
		}
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
		//	BUSCA OS DADOS
		$row = $this->select($id);

		$sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
		parent::executaSQL($sql);

		//	ARMAZENA O LOG
		parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
		if($id != "")
		{
			$sql = "
			SELECT
			*
			FROM
			" . $this->nome_tabela. "
			WHERE
			" . $this->chave_tabela. " = '$id'
			";
			return mysql_fetch_array(parent::executaSQL($sql));
		}
		else
		{
			$sql = "
			SELECT
			*
			FROM
			" . $this->nome_tabela. "
			ORDER BY
			ordem desc
			";
			return parent::executaSQL($sql);
		}

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
		$obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
		$obj_imagem->redimensiona($largura, $altura, $tipo);
		$obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
