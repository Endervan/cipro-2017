<!--	====================================================================================================	 -->
<!--	LIMPAR CACHE	 -->
<!--	====================================================================================================	 -->
<?php
header("Pragma: no-cache");
header("Cache: no-cache");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
?>


<!--	====================================================================================================	 -->
<!--	META TAGS	 -->
<!--	====================================================================================================	 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="Sigra" />
<meta name="keywords" content="Sigra" />
<meta name="revisit-after" content="7 Days" />
<meta name="language" content="pt-br" />
<meta name="robots" content="all" />
<meta name="rating" content="general" />
<meta name="copyright" content="Copyright Tekan www.tekan.com.br" />
<link rel="index" href="http://www.sigra.com.br/sitemap.xml" />



<!--	====================================================================================================	 -->
<!--	TITULO DO SITE	 -->
<!--	====================================================================================================	 -->
<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>



<link href="<?php echo Util::caminho_projeto(); ?>/admin/css/dashboard.css" rel="stylesheet">


<!-- Bootstrap core CSS -->
<link href="<?php echo Util::caminho_projeto(); ?>/css/bootstrap-3.7.min.css" rel="stylesheet">


<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<link href="<?php echo Util::caminho_projeto(); ?>/admin/css/style.css" rel="stylesheet" type="text/css" media="screen" />




<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/css/non-responsive.css" rel="stylesheet">





<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery-2.2.4.min.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-3.7.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Util::caminho_projeto() ?>/js/ie10-viewport-bug-workaround.js"></script>



<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-select.min.js"></script>



<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrapValidator.min.css"/>
 <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/font-awesome.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/js/bootstrap-lightbox.min.js"></script>




<script>

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>





<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
    window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>


<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/js/jquery.flexslider-min.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.mousewheel.js"></script>
<!-- <script defer src="<?php // echo Util::caminho_projeto() ?>/js/demo.js"></script> -->


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/flexslider.css" type="text/css" media="screen" />




  <!--  LAYER SLIDER - -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/css/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/js/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true,
            autoplay: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->




    <!--    ====================================================================================================     -->
    <!--    CLASSE DE FONTS DO SITE   -->
    <!--    ====================================================================================================     -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">




<script>
$(document).ready(function() {
    $('.FormBusca')
        .bootstrapValidator({
            container: 'tooltip',
            feedbackIcons: {
              valid: 'fa fa-check',
              invalid: 'fa fa-remove',
              validating: 'fa fa-refresh'
            },
            fields: {
                busca_produtos: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor, preêncha esse campo.'
                        }
                    }
                }
            }
        })
        .on('error.field.bv', function(e, data) {
            // Get the tooltip
            var $parent = data.element.parents('.form-group'),
                $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]'),
                title   = $icon.data('bs.tooltip').getTitle();

            // Destroy the old tooltip and create a new one positioned to the right
            $icon.tooltip('destroy').tooltip({
                html: true,
                placement: 'right',
                title: title,
                container: 'body'
            });
        });

    // Reset the Tooltip container form
    $('#resetButton').on('click', function(e) {
        var fields = $('#tooltipContainerForm').data('bootstrapValidator').getOptions().fields,
            $parent, $icon;

        for (var field in fields) {
            $parent = $('[name="' + field + '"]').parents('.form-group');
            $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + field + '"]');
            $icon.tooltip('destroy');
        }

        // Then reset the form
        $('#tooltipContainerForm').data('bootstrapValidator').resetForm(true);
    });
});
</script>







<?php /*
<!-- ======================================================================= -->
<!-- multiplo upload http://plugins.krajee.com/file-input   -->
<!-- ======================================================================= -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/kartik-v-bootstrap-fileinput-32b0ccb/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/jquery/kartik-v-bootstrap-fileinput-32b0ccb/js/fileinput.js" type="text/javascript"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/kartik-v-bootstrap-fileinput-32b0ccb/js/fileinput_locale_fr.js" type="text/javascript"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/kartik-v-bootstrap-fileinput-32b0ccb/js/fileinput_locale_es.js" type="text/javascript"></script>



<!-- ======================================================================= -->
<!-- exemplo de uso    -->
<!-- ======================================================================= -->
<!--


<script>
    $(document).ready(function() {
        $("#file-3").fileinput({
            showUpload: false,
            showCaption: false,
            browseClass: "btn btn-primary btn-lg",
            fileType: "any",
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>"
        });
    });
</script>


<div class="col-xs-12 form-group">
     <label>Galeria de imagens</label>
     <input id="file-3" type="file" multiple=true>
</div>


-->
<!-- ======================================================================= -->
<!-- exemplo de uso    -->
<!-- ======================================================================= -->

*/ ?>



<!-- ======================================================================= -->
<!-- PESQUISA DAS TABELAS    -->
<!-- ======================================================================= -->
<script>
  $(document).ready(function () {

    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

});
</script>
<!-- ======================================================================= -->
<!-- PESQUISA DAS TABELAS    -->
<!-- ======================================================================= -->

<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.maskMoney.0.2.js" type="text/javascript"></script>
